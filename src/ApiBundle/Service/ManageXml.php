<?php

/**
 * @author  Florindo López
 */

namespace ApiBundle\Service;

use DOMDocument;
use ApiBundle\Exception\ApiException;
use ApiBundle\Exception\NackException;

class ManageXml
{
    /**
     * @var string
     */
    protected $xsdPath;

    /**
     * @var string
     */
    protected $xmlStr;

    /**
     * @var string
     */
    protected $xsdFile;

    public function __construct()
    {
        $this->xsdPath  = __DIR__.'/../Resources/public/xsds/';
    }

    /**
     * Set $xsdPath params
     * 
     * @throws ApiException
     * 
     * @param string $xsdPath
     * 
     * @return ManageXml
     */
    public function setXsdPath($xsdPath)
    {
        if (!$xsdPath) {
            throw new ApiException(ApiException::NOT_FOUND, sprintf('xsdPath not found.'));
        }
        $this->xsdPath = $xsdPath;
        
        return $this;
    }

    /**
     * Set $xmlStr params
     * 
     * @throws ApiException
     * 
     * @param string $xmlStr
     * 
     * @return ManageXml
     */
    public function setXMLStr($xmlStr)
    {
        if (!$xmlStr) {
            throw new ApiException(ApiException::NOT_FOUND, sprintf('XML not found. xml parameter is mandatory'));
        }
        $this->xmlStr = $xmlStr;
        
        return $this;
    }

    /**
     * Set $xsdFile params
     * 
     * @throws ApiException
     * 
     * @param string $xsdFile
     * 
     * @return ManageXml
     */
    public function setXSDFile($xsdFile)
    {
        $xsdFile = $this->xsdPath.$xsdFile;

        if (!is_file($xsdFile)) {
            throw new ApiException(ApiException::NOT_FOUND, sprintf('XSD file `%s` not found.', $xsdFile));
        }
        $this->xsdFile = $xsdFile;

        return $this;
    }
    
    /**
     * Load and validate xml
     * 
     * @throws ApiException
     * 
     * @param string $xmlStr
     * @param string $xsdFile
     * 
     * @return DOMDocument
     */
    public function loadDOM($xmlStr, $xsdFile)
    {
        $this->setXMLStr($xmlStr);
        $this->setXSDFile($xsdFile);

        $xmlDOM = new DOMDocument(); 
        try {
            $xmlDOM->loadXml($this->xmlStr);
        } catch (\Exception $e) {
            throw new ApiException(ApiException::BAD_REQUEST, $e->getMessage());
        }

        return $xmlDOM;
    }
    
    /**
     * Create empty DOMDocument
     * 
     * @return DOMDocument
     */
    public function createDOM()
    {
        return new DOMDocument( "1.0", "UTF-8" );
    }
    
    /**
     * Validate xmlDom with xsd schema
     * 
     * @throws NackException
     * 
     * @param DOMDocument $xmlDOM
     * 
     * @return ManageXml
     */
    public function validate($xmlDOM)
    {
        libxml_use_internal_errors(true);

        if (!$xmlDOM->schemaValidate($this->xsdFile)) {
            $errors = $this->getXMLErrorsString();
            throw new NackException(NackException::BAD_REQUEST, sprintf("Document XML does not validate XSD file :%s", $errors), $xmlDOM);
        }

        return $this;
    }
    
    /**
     * Parse error to string format
     * 
     * @return string
     */
    public function getXMLErrorsString()
    {
        $errorsString = '';
        $errors = libxml_get_errors();

        foreach ($errors as $key => $error) {
            $level = $error->level === LIBXML_ERR_WARNING? 'Warning' : $error->level === LIBXML_ERR_ERROR? 'Error' : 'Fatal';
            $errorsString .= sprintf(" [%s] %s", $level, $error->message);
        }

        return $errorsString;
    }
}