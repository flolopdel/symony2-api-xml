<?php

namespace ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;


/**
 * @author  Florindo López
 */
class ApiController extends FOSRestController
{

    /**
     * Manage ping request and response
     *
     * @return Response xml
     */
    public function sendPingAction(Request $request)
    {

        $pingRepository   = $this->get('api.repository.ping');

		$xmlStr           = $request->request->get('xml');
        
        $pingResponse     = $pingRepository->loadXmlStr($xmlStr);
        $xml              = $pingRepository->getXMLMessage($pingResponse);
        
        $response = new Response($xml);
        $response->headers->set('Content-Type', 'text/xml');
        return $response;
    }
    
    /**
     * Manage reverse request and response
     *
     * @return Response xml
     */
    public function sendReverseAction(Request $request)
    {

        $pingRepository   = $this->get('api.repository.reverse');

        $xmlStr           = $request->request->get('xml');
        
        $reverseResponse  = $pingRepository->loadXmlStr($xmlStr);
        $xml              = $pingRepository->getXMLMessage($reverseResponse);
        
        $response = new Response($xml);
        $response->headers->set('Content-Type', 'text/xml');
        return $response;

    }
}
