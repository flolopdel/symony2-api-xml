<?php

namespace ApiBundle\Classes;

use ApiBundle\Classes\HeaderMessage;

/**
 * @author  Florindo López
 */
class ReverseMessage extends HeaderMessage
{
    const VALIDATE_REQUEST  = "reverse_request.xsd";
    const VALIDATE_RESPONSE = "reverse_response.xsd";

    /**
     * @var string
     */
    private $string;

    /**
     * @var string
     */
    private $reverse;

    /**
     *
     * @return string
     */
    public function getString()
    {
        return $this->string;
    }

    /**
     * 
     * @param string $echo
     * @return HeaderMessage
     */
    public function setString($string)
    {
        $this->string = $string;
        $this->reverse = strrev($string);
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getReverse()
    {
        return $this->reverse;
    }

    /**
     * 
     * @param string $reverse
     * @return HeaderMessage
     */
    public function setReverse($reverse)
    {
        $this->reverse = $reverse;
        return $this;
    }


}
