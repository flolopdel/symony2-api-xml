<?php

namespace ApiBundle\Classes;

use ApiBundle\Classes\HeaderMessage;

/**
 * @author  Florindo López
 */
class NackMessage extends HeaderMessage
{
    const VALIDATE_RESPONSE = "nack.xsd";

    /**
     * @var integer
     */
    private $code;

    /**
     * @var string
     */
    private $message;

    /**
     *
     * @return integer
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * 
     * @param integer $code
     * @return integer
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * 
     * @param string $message
     * @return integer
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }


}
