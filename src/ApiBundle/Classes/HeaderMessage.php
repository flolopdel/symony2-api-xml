<?php

namespace ApiBundle\Classes;

/**
 * @author  Florindo López
 */
class HeaderMessage
{
    /**
     * @var string
     */
    protected $type;
    
    /**
     * @var string
     */
    protected $sender;
    
    /**
     * @var string
     */
    protected $recipient;
    
    /**
     * @var string
     */
    protected $reference;
    
    /**
     * @var string
     */
    protected $timestamp;

    /**
     *
     * @return type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * 
     * @param string $type
     * @return HeaderMessage
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     *
     * @return sender
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * 
     * @param string $sender
     * @return HeaderMessage
     */
    public function setSender($sender)
    {
        $this->sender = $sender;
        return $this;
    }

    /**
     *
     * @return recipient
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * 
     * @param string $recipient
     * @return HeaderMessage
     */
    public function setRecipient($recipient)
    {
        $this->recipient = $recipient;
        return $this;
    }

    /**
     *
     * @return reference
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * 
     * @param string $reference
     * @return HeaderMessage
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
        return $this;
    }

    /**
     *
     * @return timestamp
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * 
     * @param string $timestamp
     * @return HeaderMessage
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
        return $this;
    }
}
