<?php

namespace ApiBundle\Classes;

use ApiBundle\Classes\HeaderMessage;

/**
 * @author  Florindo López
 */
class PingMessage extends HeaderMessage
{
    const VALIDATE_REQUEST  = "ping_request.xsd";
    const VALIDATE_RESPONSE = "ping_response.xsd";

    /**
     * @var string
     */
    private $echo;

    /**
     *
     * @return echo
     */
    public function getEcho()
    {
        return $this->echo;
    }

    /**
     * 
     * @param string $echo
     * @return HeaderMessage
     */
    public function setEcho($echo)
    {
        $this->echo = $echo;
        return $this;
    }


}
