<?php

namespace ApiBundle\Repository;

/**
 * BaseRepository
 */
class BaseRepository
{

    /**
     * @var \ApiBundle\Classes\HeaderMessage
     */
    protected $message;

    /**
     * @return HeaderMessage
     */
    public function getMessageClass(){

        return $this->message;
    }

    /**
     * Load and validate xml request
     * 
     * @param string $xmlStr
     * 
     * @return HeaderMessage
     */
    public function loadXmlStr($xmlStr){
        $messageClass = $this->message;

        $xmlDOM     = $this->manageXml->loadDOM($xmlStr, $messageClass::VALIDATE_REQUEST);
        $validate   = $this->manageXml->validate($xmlDOM);

        $this->setHeaderMessage($xmlDOM);
        $this->setBodyMessage($xmlDOM);

        return $this->message;
    }

    /**
     * Get xml response from different Type Class and validate
     * 
     * @param HeaderMessage $messageClass
     * 
     * @return string $xmlResponse
     */
    public function getXMLMessage($messageClass){
        $xmlResponse = $this->parseXMLMessage($messageClass);

        $xmlDOM     = $this->manageXml->loadDOM($xmlResponse, $messageClass::VALIDATE_RESPONSE);
        $validate   = $this->manageXml->validate($xmlDOM);

        return $xmlResponse;
    }

    /**
     * Parse DOM to HeaderMessage
     * 
     * @param DOMDocument $xmlDOM
     */
    public function setHeaderMessage($xmlDOM){

        if($xmlDOM->getElementsByTagName( "header" )->length > 0 ){
            $headerDOM  = $xmlDOM->getElementsByTagName( "header" )->item(0);  

            if($headerDOM->getElementsByTagName( "type" )->length > 0 ){
                $type = str_replace("_request", "_response", $headerDOM->getElementsByTagName( "type" )->item(0)->nodeValue);
                $this->message->setType($type);
            }
            if($headerDOM->getElementsByTagName( "recipient" )->length > 0 ){
                $this->message->setSender($headerDOM->getElementsByTagName( "recipient" )->item(0)->nodeValue);
            }
            if($headerDOM->getElementsByTagName( "sender" )->length > 0 ){
                $this->message->setRecipient($headerDOM->getElementsByTagName( "sender" )->item(0)->nodeValue);
            }
            if($headerDOM->getElementsByTagName( "reference" )->length > 0 ){
                $this->message->setReference($headerDOM->getElementsByTagName( "reference" )->item(0)->nodeValue);
            }
            $this->message->setTimestamp(date("c", time()));
        }
    }

    /**
     * @param xmlDOM 
     */
    public function setBodyMessage($xmlDOM){
        //Nothing in headerMessage
    }

    /**
     * Parse HeaderMessage to xml response
     * 
     * @param HeaderMessage $message
     * 
     * @return xml
     */
    public function parseXMLMessage($message){

        $xmlDOM     = $this->getHeaderDOM($message);

        $xmlBody    = $xmlDOM->createElement( "body" );

        $xmlType = $xmlDOM->getElementsByTagName( $message->getType() )->item(0);
        $xmlType->appendChild( $xmlBody );

        return $xmlDOM->saveXML();
    }

    /**
     * Create common Header DOM
     * 
     * @param HeaderMessage $message
     * 
     * @return DOMDocument
     */
    public function getHeaderDOM($message){

        $xmlDOM     = $this->manageXml->createDOM();

        $xml        = $xmlDOM->createElement( $message->getType());

        $xmlHeader  = $xmlDOM->createElement( "header" );

        $xmlType        =  $xmlDOM->createElement( "type",  $message->getType());
        $xmlSender      =  $xmlDOM->createElement( "sender",  $message->getSender());
        $xmlRecipient   =  $xmlDOM->createElement( "recipient",  $message->getRecipient());
        $xmlReference    =  $xmlDOM->createElement( "reference",  $message->getReference());
        $xmlTimestamp    =  $xmlDOM->createElement( "timestamp",  $message->getTimestamp());

        $xmlHeader->appendChild( $xmlType );
        $xmlHeader->appendChild( $xmlSender );
        $xmlHeader->appendChild( $xmlRecipient );
        $xmlHeader->appendChild( $xmlReference );
        $xmlHeader->appendChild( $xmlTimestamp );

        $xml->appendChild( $xmlHeader );

        $xmlDOM->appendChild( $xml );

        return $xmlDOM;
    }

    /**
     * Create common Error DOM
     * 
     * @param DOMDocument $xmlDOM
     * @param string $errorMessage
     * @param integer $errorCode
     * 
     * @return DOMDocument
     */
    public function getErrorDOM($xmlDOM, $errorMessage, $errorCode){

        $xmlError       = $xmlDOM->createElement( "error" );
        $xmlCode        =  $xmlDOM->createElement( "code",  $errorCode);
        $xmlMessage     =  $xmlDOM->createElement( "message",  $errorMessage);

        $xmlError->appendChild( $xmlCode );
        $xmlError->appendChild( $xmlMessage );

        return $xmlError;
    }
}
