<?php

namespace ApiBundle\Repository;

use ApiBundle\Classes\NackMessage;
use ApiBundle\Service\ManageXml;
use DOMDocument;

/**
 * NackRepository
 */
class NackRepository extends BaseRepository
{

    protected $manageXml;

    /**
     * Create new instance of NackRepository
     * 
     * @param DOMDocument $xmlDOM
     * @param integer $errorCode
     * @param string $errorMessage
     * 
     */
    public function __construct(DOMDocument $xmlDOM, $errorCode, $errorMessage)
    {
        $this->message      = new NackMessage();
        $this->manageXml    = new ManageXml();

        parent::setHeaderMessage($xmlDOM);
        $this->message->setType("nack");
        $this->message->setCode($errorCode);
        $this->message->setMessage($errorMessage);

    }

    /**
     * Parse NackMessage to xml response
     * 
     * @param NackMessage $message
     * 
     * @return xml
     */
    public function parseXMLMessage($message){

        $xmlDOM     = parent::getHeaderDOM($message);

        $xmlBody    = $xmlDOM->createElement( "body" );

        $xmlError   = parent::getErrorDOM($xmlDOM, $message->getMessage(), $message->getCode());
        $xmlBody->appendChild( $xmlError );

        $xmlType    = $xmlDOM->getElementsByTagName( $message->getType() )->item(0);
        $xmlType->appendChild( $xmlBody );

        return $xmlDOM->saveXML();
    }
}
