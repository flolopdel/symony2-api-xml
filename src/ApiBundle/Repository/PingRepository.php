<?php

namespace ApiBundle\Repository;

use ApiBundle\Service\ManageXml;
use ApiBundle\Classes\PingMessage;

/**
 * PingRepository
 */
class PingRepository extends BaseRepository
{

    protected $manageXml;


    public function __construct(ManageXml $manageXml, PingMessage $pingMessage)
    {
        $this->manageXml        = $manageXml;
        $this->message          = $pingMessage;
    }

    /**
     * Parse DOM to PingMessage
     * 
     * @param DOMDocumnet $xmlDOM
     *  
     */
    public function setBodyMessage($xmlDOM){

        if($xmlDOM->getElementsByTagName( "body" )->length > 0){
            
            $bodyDOM = $xmlDOM->getElementsByTagName( "body" )->item(0);
            if($bodyDOM->getElementsByTagName( "echo" )->length > 0){
                $this->message->setEcho($bodyDOM->getElementsByTagName( "echo" )->item(0)->nodeValue);
            }
        }
    }

    /**
     * Parse PingMessage to xml response
     * 
     * @param PingMessage $message
     * 
     * @return xml
     */
    public function parseXMLMessage($message){

        $xmlDOM     = parent::getHeaderDOM($message);

        $xmlBody    = $xmlDOM->createElement( "body" );
        $xmlEcho    = $xmlDOM->createElement( "echo",  $message->getEcho());
        $xmlBody->appendChild( $xmlEcho );

        if(!$message->getEcho()){
            $xmlError = parent::getErrorDOM($xmlDOM, "echo content is empty", 400);
            $xmlBody->appendChild( $xmlError );
        }

        $xmlType = $xmlDOM->getElementsByTagName( $message->getType() )->item(0);
        $xmlType->appendChild( $xmlBody );

        return $xmlDOM->saveXML();
    }
}
