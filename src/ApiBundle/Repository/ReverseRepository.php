<?php

namespace ApiBundle\Repository;

use ApiBundle\Service\ManageXml;
use ApiBundle\Classes\ReverseMessage;

/**
 * ReverseRepository
 */
class ReverseRepository extends BaseRepository
{

    protected $manageXml;


    public function __construct(ManageXml $manageXml, ReverseMessage $message)
    {
        $this->manageXml        = $manageXml;
        $this->message          = $message;
    }

    /**
     * Parse DOM to ReverseMessage
     * 
     * @param DOMDocumnet $xmlDOM
     *  
     */
    public function setBodyMessage($xmlDOM){

        if($xmlDOM->getElementsByTagName( "body" )->length > 0){
            
            $bodyDOM = $xmlDOM->getElementsByTagName( "body" )->item(0);
            if($bodyDOM->getElementsByTagName( "string" )->length > 0){
                $this->message->setString($bodyDOM->getElementsByTagName( "string" )->item(0)->nodeValue);
            }
        }
    }

    /**
     * Parse ReverseMessage to xml response
     * 
     * @param ReverseMessage $message
     * 
     * @return xml
     */
    public function parseXMLMessage($message){

        $xmlDOM     = parent::getHeaderDOM($message);

        $xmlBody    = $xmlDOM->createElement( "body" );
        $xmlString  = $xmlDOM->createElement( "string",  $message->getString());
        $xmlReverse = $xmlDOM->createElement( "reverse",  $message->getReverse());

        $xmlBody->appendChild( $xmlString );
        $xmlBody->appendChild( $xmlReverse );

        if(!$message->getString()){
            $xmlError = parent::getErrorDOM($xmlDOM, "string content is empty", 400);
            $xmlBody->appendChild( $xmlError );
        }

        $xmlType = $xmlDOM->getElementsByTagName( $message->getType() )->item(0);
        $xmlType->appendChild( $xmlBody );

        return $xmlDOM->saveXML();
    }
}
