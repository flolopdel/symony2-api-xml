<?php

namespace ApiBundle\Listener;

use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use ApiBundle\Exception\ApiException;
use ApiBundle\Exception\NackException;

/**
 * ExceptionListener.
 *
 * @author  Florindo López
 */
class ExceptionListener
{
	
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        // You get the exception object from the received event
        $exception = $event->getException();
        // Customize your response object to display the exception details

        if ($exception instanceof ApiException || $exception instanceof NackException) {
            $response = $exception->getResponseBody();
            $event->setResponse($response);
        }

    }
}