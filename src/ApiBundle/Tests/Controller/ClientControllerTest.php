<?php

namespace ApiBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ClientControllerTest extends WebTestCase
{
    
    public function testSendPing()
    {
        // Create a new client to browse the application
        $client = static::createClient();

        // 200 ERROR case
       $client->request('POST', '/api/ping', array('xml' => '<?xml version="1.0" encoding="UTF-8"?>
        <ping_request>
          <header>
            <type>ping_request</type>
            <sender>DEMO</sender>
            <recipient>VOICEWORKS</recipient>
            <reference>ping_request_12345</reference>
            <timestamp>2013-12-19T16:45:11.050+01:00</timestamp>
          </header>
          <body>
            <echo>Hello!</echo>
          </body>
        </ping_request>
        '));
        $this->assertEquals(200, $client->getResponse()->getStatusCode(), "Unexpected HTTP status code for POST /api/ping/");

        // 400 ERROR case
        $client->request('POST', '/api/ping', array('xml' => '<?xml version="1.0" encoding="UTF-8"?>
        <ping_request>
          <header>
            <type>ping_request</type>
            <recipient>VOICEWORKS</recipient>
            <reference>ping_request_12345</reference>
            <timestamp>2013-12-19T16:45:11.050+01:00</timestamp>
          </header>
          <body>
            <echo>Hello!</echo>
          </body>
        </ping_request>
        '));
        $this->assertEquals(400, $client->getResponse()->getStatusCode(), "Unexpected HTTP status code for POST /api/ping/");

        // 404 ERROR case
        $client->request('POST', '/api/ping');
        $this->assertEquals(404, $client->getResponse()->getStatusCode(), "Unexpected HTTP status code for POST /api/ping/");
    }
    
    public function testReversePing()
    {
        // Create a new client to browse the application
        $client = static::createClient();

        // 200 ERROR case
       $client->request('POST', '/api/reverse', array('xml' => '<?xml version="1.0" encoding="UTF-8"?>
        <reverse_request>
          <header>
            <type>reverse_request</type>
            <sender>VOICEWORKS</sender>
            <recipient>DEMO</recipient>
            <reference>reverse_request_12345</reference>
            <timestamp>2013-12-19T16:45:10.950+01:00</timestamp>
          </header>
          <body>
            <string>Hello!</string>
          </body>
        </reverse_request>
        '));
        $this->assertEquals(200, $client->getResponse()->getStatusCode(), "Unexpected HTTP status code for POST /api/reverse/");

        // 400 ERROR case
        $client->request('POST', '/api/reverse', array('xml' => '<?xml version="1.0" encoding="UTF-8"?>
        <reverse_request>
          <header>
            <type>ping_request</type>
            <sender>VOICEWORKS</sender>
            <recipient>DEMO</recipient>
            <reference>reverse_request_12345</reference>
            <timestamp>2013-12-19T16:45:10.950+01:00</timestamp>
          </header>
          <body>
            <string>Hello!</string>
          </body>
        </reverse_request>
        '));
        $this->assertEquals(400, $client->getResponse()->getStatusCode(), "Unexpected HTTP status code for POST /api/reverse/");

        // 404 ERROR case
        $client->request('POST', '/api/reverse');
        $this->assertEquals(404, $client->getResponse()->getStatusCode(), "Unexpected HTTP status code for POST /api/reverse/");
    }

    
}
