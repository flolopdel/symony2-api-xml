<?php

namespace ApiBundle\Exception;

use Symfony\Component\HttpFoundation\Response;
use ApiBundle\Repository\NackRepository;
use Symfony\Component\HttpKernel\Exception\HttpException;

use DOMDocument;

/**
 * NackException.
 *
 * @author Florindo López
 */
class NackException extends HttpException
{
    const BAD_REQUEST    = 400;

    /**
     * @var mixed
     */
	private $errorCode;

    /**
     * @var mixed
     */
    private $xmlDOM;
	
    
    /**
     * 
     * @param mixed $errorCode
     * @param string $errorMessage
     * @param DOMDocument $xmlDOM
     */
    public function __construct($errorCode, $errorMessage, DOMDocument $xmlDOM)
    {
    	$this->errorCode = $errorCode;
        $this->xmlDOM = $xmlDOM;

        parent::__construct($errorCode, $errorMessage);
    }
    
    /**
     * 
     * @return string
     */
    public function getErrorCode(){
    	return $this->errorCode;
    }
    
    /**
     * Create xml response with nack error format
     * 
     * @return Response Xml
     */
    public function getResponseBody(){
        $nackRepository = new NackRepository($this->xmlDOM, $this->getErrorCode(), $this->getMessage());
        
        $messageClass   = $nackRepository->getMessageClass();
        $xmlResponse    = $nackRepository->getXMLMessage( $messageClass);

        $response = new Response($xmlResponse);
        $response->headers->set('Content-Type', 'text/xml');
        return $response;
    }

}
