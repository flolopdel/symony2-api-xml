<?php

namespace ApiBundle\Exception;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

use DOMDocument;

/**
 * ApiException.
 *
 * @author Florindo López
 */
class ApiException extends HttpException
{
    const NOT_FOUND      = 404;
    const BAD_REQUEST    = 400;

    /**
     * @var mixed
     */
	private $errorCode;
	
    /**
     * 
     * @param mixed $errorCode
     * @param string $errorMessage
     */
    public function __construct($errorCode, $errorMessage)
    {
    	$this->errorCode = $errorCode;
        parent::__construct($errorCode, $errorMessage);
    }
    
    /**
     * 
     * @return string
     */
    public function getErrorCode(){
    	return $this->errorCode;
    }
    
    /**
     * Create xml response with general error format
     * 
     * @return Response xml
     */
    public function getResponseBody(){
        $xmlDOM     = new DOMDocument( "1.0", "UTF-8" );
        $xmlError   = $xmlDOM->createElement( "error");

        $xmlCode        =  $xmlDOM->createElement( "code",  $this->getErrorCode());
        $xmlMessage     =  $xmlDOM->createElement( "message",  $this->getMessage());

        $xmlError->appendChild( $xmlCode );
        $xmlError->appendChild( $xmlMessage );

        $xmlDOM->appendChild( $xmlError );


        $response = new Response($xmlDOM->saveXML());
        $response->headers->set('Content-Type', 'text/xml');
        return $response;
    }

}
