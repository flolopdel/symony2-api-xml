Assessment PHP Developer
========================

Symfony2 API using custom XML format and validate with XSD schemas.

Installation
============
1. composer update
1. php app/console cache:clear
2. phpunit -c app/
3. php app/console server:run

Available Method
================
1. sendPing():

* * path => /api/ping
* * params => xml request
* * return => xml response
* * method => post

2. sendReverse():

* * path => /api/reverse
* * params => xml request
* * return => xml response
* * method => post

Exception and Error
===================
1. **Error codes**: 400, 404
2. **ApiException**: Exception throws when the params xml is not found or it can not be loaded for a wrong xml format.
3. **NackException**: Exception throws when the xml has been already loaded but it doesn´t pass the xsd validation and de request can not be process.
4. **Body Error**: In the documentation I have read about a optional error nodo into the body nodo. I don´t understand when this error is throws, I have decided throws this error when the echo o string nodo is empty.

Test Cases
==========
There are 6 different test cases into ClientControllerTest.php.

1. /api/ping expected 200 code with a correct xml_request.
2. /api/ping expected 400 code with a wrong xml_request.
3. /api/ping expected 404 code without a xml parameter.
4. /api/reverse expected 200 code with a correct xml_request.
5. /api/reverse expected 400 code with a wrong xml_request.
6. /api/reverse expected 404 code without a xml parameter.

To check this test cases you can use phpunit

```
#!php

phpunit -c app/
```